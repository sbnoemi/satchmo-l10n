from l10n.models import Country, AdminArea, CurrencyCode
from django.contrib import admin
from django.utils.translation import get_language, ugettext_lazy as _

def make_enable(modeladmin, request, queryset):
    rows_updated = queryset.update(active=True)
    if rows_updated == 1:
        message_bit = _("1 story was")
    else:
        message_bit = "%s stories were" % rows_updated
    make_enable.short_description = _("Mark selected stories as enable")

def make_disable(modeladmin, request, queryset):
    rows_updated = queryset.update(active=False)
    if rows_updated == 1:
        message_bit = _("1 story was")
    else:
        message_bit = "%s stories were" % rows_updated
    make_disable.short_description = _("Mark selected stories as disable")

class AdminArea_Inline(admin.TabularInline):
    model = AdminArea
    extra = 1

class CountryOptions(admin.ModelAdmin):
    list_display = ('printable_name', 'iso2_code', 'active',)
    list_filter = ('continent', 'active')
    search_fields = ('name', 'iso2_code', 'iso3_code')
    actions = [make_enable, make_disable]
    inlines = [AdminArea_Inline]
    save_as = True
    save_on_top = True
    list_per_page = 50

class CurrencyCodeOptions(admin.ModelAdmin):
    list_display = ('iso3_code', 'name', 'symbol', 'active',)
    list_filter = ('active',)
    search_fields = ('name', 'numcode', 'iso3_code')
    actions = [make_enable, make_disable]
    save_as = True
    save_on_top = True
    list_per_page = 50
    
admin.site.register(Country, CountryOptions)
admin.site.register(CurrencyCode, CurrencyCodeOptions)
