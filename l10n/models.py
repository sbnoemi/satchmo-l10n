# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _

CONTINENTS = (
    ('AF', _('Africa')),
    ('NA', _('North America')),
    ('EU',  _('Europe')),
    ('AS', _('Asia')),
    ('OC',  _('Oceania')),
    ('SA', _('South America')),
    ('AN', _('Antarctica'))
)

AREAS = (
    ('a', _('Another')),
    ('i', _('Island')),
    ('ar', _('Arrondissement')),
    ('at', _('Atoll')),
    ('ai', _('Autonomous island')),
    ('ca', _('Canton')),
    ('cm', _('Commune')),
    ('co', _('County')),
    ('dp', _('Department')),
    ('de', _('Dependency')),
    ('dt', _('District')),
    ('dv', _('Division')),
    ('em', _('Emirate')),
    ('gv', _('Governorate')),
    ('ic', _('Island council')),
    ('ig', _('Island group')),
    ('ir', _('Island region')),
    ('kd', _('Kingdom')),
    ('mu', _('Municipality')),
    ('pa', _('Parish')),
    ('pf', _('Prefecture')),
    ('pr', _('Province')),
    ('rg', _('Region')),
    ('rp', _('Republic')),
    ('sh', _('Sheading')),
    ('st', _('State')),
    ('sd', _('Subdivision')),
    ('sj', _('Subject')),
    ('ty', _('Territory')),
)



class Country(models.Model):
    """
    International Organization for Standardization (ISO) 3166-1 Country list
    """
    iso2_code = models.CharField(_('ISO alpha-2'), max_length=2, unique=True)
    name = models.CharField(_('Official name (CAPS)'), max_length=128)
    printable_name = models.CharField(_('Country name'), max_length=128)
    iso3_code = models.CharField(_('ISO alpha-3'), max_length=3, unique=True)
    numcode = models.PositiveSmallIntegerField(_('ISO numeric'), null=True, blank=True)
    active = models.BooleanField(_('Country is active'), default=True)
    continent = models.CharField(_('Continent'), choices=CONTINENTS, max_length=2)
    admin_area = models.CharField(_('Administrative Area'), choices=AREAS, max_length=2, null=True, blank=True)

    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')
        ordering = ('name',)

    def __unicode__(self):
        return self.printable_name


class AdminArea(models.Model):
    """
    Administrative Area level 1 for a country.  For the US, this would be the states
    """
    country = models.ForeignKey(Country)
    name = models.CharField(_('Admin Area name'), max_length=60, )
    abbrev = models.CharField(_('Postal Abbreviation'), max_length=3, null=True, blank=True)
    active = models.BooleanField(_('Area is active'), default=True)

    class Meta:
        verbose_name = _('Administrative Area')
        verbose_name_plural = _('Administrative Areas')
        ordering = ('name',)

    def __unicode__(self):
        return self.name

class CurrencyCodeManager(models.Manager):
    """
    """
    def lactive(self):
        return self.filter(active=True).order_by('-orderings')

    
class CurrencyCode(models.Model):
    """
    Definitions of ISO 4217 Currencies
    Source: http://www.iso.org/iso/support/faqs/faqs_widely_used_standards/widely_used_standards_other/currency_codes/currency_codes_list-1.htm
    """
    iso3_code = models.CharField(_('ISO alpha-3'), max_length=3, unique=True)
    name = models.CharField(_('Name'), max_length=128)
    symbol = models.CharField(_('symbol'), max_length=16, null=True, blank=True)
    numcode = models.PositiveSmallIntegerField(_('ISO numeric'), null=True, blank=True)
    orderings = models.PositiveSmallIntegerField(_('Ordering'), default=1)
    active = models.BooleanField(_('Currency is active'), default=True)
    countries = models.ManyToManyField(Country, null=True, blank=True)
    objects = CurrencyCodeManager()
    
    class Meta:
        ordering = ['-orderings', 'name']
        verbose_name, verbose_name_plural = _(u"Currency"), _(u"Currencies")

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('Currencies', [self.id])

class CurrenciesManager(models.Manager):
    def get_query_set(self):
        qs = super(CurrenciesManager, self).get_query_set()
        qs = qs.filter(active=True).order_by('-orderings')
        return qs
        
    def currency_list(self):
        """"""
        return self.values_list('iso3_code', 'name')

class Currencies(CurrencyCode):
    objects = CurrenciesManager()
    
    
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        proxy = True

import config
