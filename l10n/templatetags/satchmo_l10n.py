#from django import forms
from django import template
#from django.conf import settings
from django.core import urlresolvers
#from django.utils.safestring import mark_safe
from l10n.config import config_value, config_choice_values
#from product.models import Category
#from satchmo_utils.numbers import trunc_decimal
#from satchmo_utils.json import json_encode
import logging
#import math

log = logging.getLogger('l10n.templatetags.satchmo_l10n')

register = template.Library()

@register.inclusion_tag("l10n/_language_selection_form.html", takes_context=True)
def l10n_language_selection_form(context):
    """
    Display the set language form, if enabled in shop settings.
    """
    request = context['request']
    enabled = config_value('LANGUAGE', 'ALLOW_TRANSLATION')
    languages = []
    if enabled:
        try:
            url = urlresolvers.reverse('l10n_set_language')
            languages = config_choice_values('LANGUAGE', 'LANGUAGES_AVAILABLE')
            
        except urlresolvers.NoReverseMatch:
            url = ""
            log.warning('No url found for l10n_set_language (OK if running tests)')

    else:
        url = ""
    
    media_url = context.get('media_url', None)    
    return {
        'enabled' : enabled,
        'set_language_url' : url,
        'django_language' : request.session.get('django_language', 'en'),
        'languages' : languages,
        'media_url' : media_url,
    }

@register.inclusion_tag('l10n/_language_selection_list.html', takes_context=True)
def l10n_language_selection_list(context):
    """
    Display the set language form, if enabled in shop settings.
    """
    # TODO debug_toolbar no work language menu
    request = context['request']
    enabled = config_value('LANGUAGE', 'ALLOW_TRANSLATION')
    languages = []
    if enabled:
        try:
            url = urlresolvers.reverse('l10n_set_language')
            languages = config_choice_values('LANGUAGE', 'LANGUAGES_AVAILABLE')
            
        except urlresolvers.NoReverseMatch:
            url = ""
            log.warning('No url found for l10n_set_language (OK if running tests)')

    else:
        url = ""
    
    media_url = context.get('media_url', None)    
    return {
        'enabled' : enabled,
        'set_language_url' : url,
        'django_language' : request.session.get('django_language', 'en'),
        'languages' : languages,
        'media_url' : media_url,
    }