from django.test import TestCase
from django.conf import settings
from l10n.validators import aupostcode, capostcode, uspostcode
from l10n.models import Country, CurrencyCode
from l10n.config import config_value

_missing = '*************NULL******************'

class ConfigValueTest(TestCase):
    
    def setUp(self):
       self.old_SETTINGS = getattr(settings, "NOT_SO_LIVE_SETTINGS", _missing)
       setattr(settings, 'NOT_SO_LIVE_SETTINGS', {
                    'single-level': 'single level',
                    'ima-dict': {
                        'with': 'multiple',
                        'levels': { 
                            'of': 'nesting'
                        }
                    }
                })
    
    def tearDown(self):
        if self.old_SETTINGS is _missing:
            del settings.NOT_SO_LIVE_SETTINGS
        else:
            settings.NOT_SO_LIVE_SETTINGS = self.old_SETTINGS 
            
    def test_retrieve(self):
        c = config_value('single-level')
        self.assertEqual(c, 'single level')
        c = config_value('ima-dict')
        self.assertTrue(isinstance(c, dict))
        c = config_value('ima-dict', 'levels', 'of')
        self.assertEqual(c, 'nesting')
        
        

class AUPostCodeTest(TestCase):
    def test_valid(self):
        code = aupostcode.validate("2000")
        self.assertEqual('2000', code)
        code = aupostcode.validate(" 2000 ")
        self.assertEqual('2000', code)
    
    def test_invalid(self):
        try:
            code = capostcode.validate("")
            self.fail('Invalid blank postal code not caught')
        except:
            pass
        
        try:
            code = capostcode.validate("no")
            self.fail('Invalid postal code "no" not caught')
        except:
            pass
        
class CAPostCodeTest(TestCase):
    def test_valid(self):
        code = capostcode.validate("M5V2T6")
        self.assertEqual('M5V2T6', code)
        code = capostcode.validate("m5v2t6")
        self.assertEqual('M5V2T6', code)
    
    def test_invalid(self):
        try:
            code = capostcode.validate("")
            self.fail('Invalid blank postal code not caught')
        except:
            pass
        
        try:
            code = capostcode.validate("no")
            self.fail('Invalid postal code "no" not caught')
        except:
            pass
        
        try:
            code = capostcode.validate("M5V M5V")
            self.fail('Invalid postal code "M5V M5V" not caught')
        except:
            pass
        
        try:
            code = capostcode.validate("D5V 2T6")
            self.fail('Invalid postal code "D5V 2T6" not caught -- "D" is not a valid major geographic area or province.')
        except:
            pass

class USPostCodeTest(TestCase):
    def test_five_digit(self):
        zipcode = uspostcode.validate("66044")
        self.assertEqual('66044', zipcode)
        try:
            zipcode = uspostcode.validate(" 66044 ")
            self.fail("Invalid postal code not caught")
        except:
            pass

    def test_nine_digit(self):
        zipcode = uspostcode.validate("94043-1351")
        self.assertEqual('94043-1351', zipcode)
        try:
            zipcode = uspostcode.validate(" 94043-1351 ")
            self.fail('Invalide postal code not caught')
        except:
            pass
    
    def test_invalid(self):
        try:
            code = uspostcode.validate("")
            self.fail('Invalid blank postal code not caught')
        except:
            pass
        
        try:
            zipcode = uspostcode.validate("no")
            self.fail('Invalid ZIP code "no" not caught')
        except:
            pass

class CurrencyTest(TestCase):
    fixtures = ['l10n_data']
    def test_add_currency(self):
        num_currencies = CurrencyCode.objects.lactive().count()
        c = CurrencyCode(iso3_code='ZZZ', numcode='980', name='Hryvnia')
        c.save()
        c.countries = Country.objects.filter(name__in=['UKRAINE'])
        c.save()
        self.assertEqual(num_currencies + 1, CurrencyCode.objects.lactive().count())