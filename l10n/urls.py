from django.conf.urls.defaults import *

urlpatterns = patterns('',
    (r'^setlang/$', 'django.views.i18n.set_language', {}, 'l10n_set_language'),
)
