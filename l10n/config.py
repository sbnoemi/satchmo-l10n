# -*- mode: python; coding: utf-8; -*- 
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


def config_value(*args):
    """ 
    Replacement for livesettings.config_value that relies only on hard-coded settings
    """
    val = getattr(settings, 'NOT_SO_LIVE_SETTINGS', None)
    if not val or not hasattr(val, 'get') or not callable(val.get):
        raise ConfigurationError('Settings missing, or not dict-like.')
    for key in args:
        try:
            val = val[key]
        except KeyError:
            raise ConfigurationError('Requested setting has not been set')
    return val
    
def config_choice_values(*args):
    """ 
    Replacement for livesettings.config_choice_values that relies only on hard-coded settings
    """
    return config_value(*args)
    

class ConfigurationError(Exception):
    pass
    
