import ez_setup
ez_setup.use_setuptools()

from setuptools import setup, find_packages

import os

version = __import__('l10n').__version__
setup(
    name = 'satchmo-l10n',
    version = version,
    description = "Satchmo l10n",
    long_description = """This is an app from Satchmo, language localisation.""",
    #author = 'Oleg Dolya',
    url = 'http://bitbucket.org/jbo/satchmo-l10n/',
    license = 'BSD',
    platforms = ['any'],
    scripts=['scripts/make-messages.py', 'scripts/posplit.py'],
    classifiers = ['Development Status :: 3 - Beta',
                   'Environment :: Web Environment',
                   'Framework :: Django',
                   'Intended Audience :: Developers',
                   'License :: OSI Approved :: BSD License',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                   'Topic :: Utilities'],    
    packages = find_packages(),
    include_package_data = True,
    zip_safe = False,
)
