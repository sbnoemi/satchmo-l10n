Internationalization and localization L10N
==========================================

pip install -e hg+http://bitbucket.org/jbo/satchmo-l10n/#egg=satchmo-l10n

1. Ensure that my_project/settings.py has the following specific configurations (in addition to the defaults and your other app needs):  

	INSTALLED_APPS = (
	
 		....
    	'l10n',
		....
	)
	
    NOT_SO_LIVE_SETTINGS = {
        'LANGUAGE': {
            'CURRENCY': '$',           # Change this to your local currency
            'SHOW_TRANSLATIONS': True, # Or False.  Shows translations in admin
            'ALLOW_TRANSLATION': True, # Or False.  Allows end user to choose 
                                       # from available translations on main
                                       # site
            'LANGUAGES_AVAILABLE': (   # Edit as necessary
                ('en', "English"),
                ('fr', "Français"),
                ('de',"Deutsch"),
                ('es', "Español"),
                ('he',"עִבְרִית"),
                ('it',"Italiano"),
                ('ko', "한국어"),
                ('sv', "Svenska"),
                ('pt-br',"Português"),
                ('bg',"Български"),
                ('ru',"Русский"),
                ('tr',"Türkçe"),
            )
        }
    }

2. Install the Data  
	./manage.py loaddata l10n_data  
	./manage.py loaddata currency_data

3. Load template tags in templates that use them
base.html
------------------------------------------- 

{% load satchmo_l10n %}  
{% l10n_language_selection_form %}